<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/estudiantes/nuevo', ['as' => 'student.create', 'uses' => 'StudentController@create']);
Route::get('/estudiantes', ['as' => 'student.index', 'uses' => 'StudentController@index']);
Route::post('/estudiantes/store', ['as' => 'student.store', 'uses' => 'StudentController@store']);
