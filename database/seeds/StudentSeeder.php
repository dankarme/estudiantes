<?php

use Illuminate\Database\Seeder;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i=0 ; $i<20; $i++){
            \DB::table('students')->insert([
                'name'=>$faker->name,
                'lastname'=>$faker->userName,
                'level'=>4,
                'note1'=>7,
                'note2'=>8,
                'note3'=>8,
                'note4'=>5,
                'created_at'=>date_create('now UTC'),
                'updated_at'=>date_create('now UTC')
            ]);
        }
    }
}
