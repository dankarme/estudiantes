@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h2 class="page-header">
                            <i class="fa fa-globe"></i> Listado de Estudiantes
                            <a class="btn btn-md btn-success pull-right" href="{{route('student.create')}}">  Nuevo</a>
                        </h2>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 table-responsive">
                                <table class="table table-striped">
                                    <tr>

                                        <th>ID</th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Semestre</th>
                                        <th>Esdatística</th>
                                        <th>Programacion</th>
                                        <th>Ingles</th>
                                        <th>Estructura</th>
                                        <th>Promedio</th>
                                        <th>Acciones</th>
                                    </tr>
                                    @foreach($results as $key=>$item)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->lastname}}</td>
                                            <td>{{$item->level}}</td>
                                            <td>{{$item->note1}}</td>
                                            <td>{{$item->note2}}</td>
                                            <td>{{$item->note3}}</td>
                                            <td>{{$item->note4}}</td>
                                            <td>

                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                                {!! $results->links() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
