<?php

namespace App\Http\Controllers;

use App\Note;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class StudentController extends Controller
{

    public function index()
    {
        $results = Student::paginate(env('APP_PAGINATE',10));
        /*$promedio = DB::table('students')
            ->select('note1', 'note2', 'note3', 'note4', DB::raw('SUM(note1, note2,note3,note4) as total_promedio'))
            ->groupBy('note1', 'note2', 'note3', 'note4')
            ->havingRaw('SUM(note1, note2,note3,note4) > ?', [2500])
            ->get();*/
        return view('studens.index', ['results' => $results]);
    }


    public function create()
    {
        $ord= ['1' => '1', '2' => '2','3' => '3','4' => '4','5' => '5','6' => '6'];
        return view('studens.create',['ord'=>$ord]);
    }


    public function store(Request $request)
    {
        /*try{
            DB::beginTransaction();
            $result = new Student();
            $result->fill($request->all());
           // dd($result);
            $result->save();
            DB::commit();
        }catch (\Exception $e){
            Session::flash('error', config('content.session.error'));
            DB::rollBack();
            return back()->withInput();
        }
        Session::flash('message', config('content.session.create'));
        return redirect()->route('student.index');*/

        /*$notification = new Student();
        $notification->name = $request->name;
        $notification->lastname = $request->lastname;
        $notification->level = $request->level;
        $notification->note1 = 'note1';
        $notification->note2 = 'note2';
        $notification->note3 = 'note3';
        $notification->note4 = 'note4';

        $notification->promedio = 'note4';
        $notification->save();*/
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
