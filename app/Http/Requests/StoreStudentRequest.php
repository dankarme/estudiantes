<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreStudentRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'lastname' => 'required|max:255',
            'level' => 'required',
            'note1' => 'required',
            'note2' => 'required',
            'note3' => 'required',
            'note4' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Nombre de usuario requerido',
            'name.max' => 'Nombre de usuario tiene demasiados caracteres',
            'lastname.required' => 'Nombre de usuario requerido',
            'lastname.max' => 'Nombre de usuario tiene demasiados caracteres',

        ];
    }
}
