<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected  $table='students';
    protected $fillable = [
        'id',
        'name' ,
        'lastname',
        'level',
        'note1',
        'note2',
        'note3',
        'note4',
    ];


    public function note()
    {
        return $this->hasMany(Note::class);
    }
}
