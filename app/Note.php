<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected  $table='notes';

    protected $fillable = [
        'id',
        'note1' ,
        'note2',
        'note3',
    ];

    public function Student()
    {
        return $this->belongsTo('App\Student','id');
    }
}
